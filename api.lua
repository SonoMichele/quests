local yaml = dofile(minetest.get_modpath("quests") .. "/yaml.lua")
local quests_list = {}

function quests.load_quests()
  quests_list = {}
  local dir = minetest.get_modpath("quests") .. "/quests"
  local files = minetest.get_dir_list(dir)

  local function endswith(str, ending)
    return ending == "" or str:sub(-#ending) == ending
  end

  for _, f_name in pairs(files) do
    if endswith(f_name, ".yml") or endswith(f_name, ".yaml") then
      local file = io.open(dir .. "/".. f_name, "r")
      local content = yaml.parse(file:read("*all"))
      for id, quest in pairs(content) do
        quests_list[id] = quest
      end
    end
  end

end


function quests.get_quest(quest_id)
  return quests_list[quest_id] or nil
end


function quests.get_player_completed_quests(p_name)
  return quests.backend.get_player_completed_quests(p_name)
end


function quests.get_player_active_quests(p_name)
  return quests.backend.get_player_active_quests(p_name)
end


--[[ Return values
-1, nil: quest doesn't exist
0, nil: quest already started
1, nil: quest started
2, {quest_id=false,}: player doesn't meet requirements
]]
function quests.start_quest(p_name, quest_id)
  local quest = quests.get_quest(quest_id)
  if not quest then return -1, nil end

  if not quest.required_quests then
    -- minetest.log("Niente requisiti")
    local started = quests.backend.start_quest(p_name, quest_id)
    if started then return 1, nil end
    return 0, nil
  end

  local not_completed = {}
  local count = 0

  for _, req_quest_id in pairs(quest.required_quests) do
    if not quests.get_player_completed_quests(p_name)[req_quest_id] then
      not_completed[req_quest_id] = false
      count = count + 1
    end
  end

  -- minetest.log("Quests non completate tab " .. dump(not_completed))
  -- minetest.log("Quest non completate" .. count)
  if count > 0 then return 2, not_completed end

  local started = quests.backend.start_quest(p_name, quest_id)
  if started then return 1, nil end
  return 0, nil
end


--[[ Return values
-1: quest doesn't exist
0: quest isn't active
1: moved to next stage
2: The player completed all stages and the quest has been completed
]]
function quests.next_stage(p_name, quest_id)
  local quest = quests.get_quest(quest_id)
  if not quest then return -1 end

  local current_stage = quests.backend.get_quest_stage(p_name, quest_id)
  if current_stage < 0 then return 0 end

  if current_stage + 1 > #quest.stages then
    quests.backend.set_quest_stage(p_name, quest_id, current_stage + 1)
    quests.complete_quest(p_name, quest_id)
    return 2
  end

  quests.backend.set_quest_stage(p_name, quest_id, current_stage + 1)
  return 1
end


--[[ Return values
-1: quest doesn't exist
0: quest already completed
1: quest completed
2: player hasn't completed all stages
]]
function quests.complete_quest(p_name, quest_id)
  local quest = quests.get_quest(quest_id)
  if not quest then return -1 end

  if quests.backend.get_quest_stage(p_name, quest_id) > #quest.stages then
    local completed = quests.backend.complete_quest(p_name, quest_id)
    if completed then return 1 end
    return 0
  end

  return 2
end
