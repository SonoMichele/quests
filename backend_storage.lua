local storage = minetest.get_mod_storage()
--[[
  p_name: {
    completed: {
      1: true,
      2: true,
    },
    active: {
      3: {
        stage: 1
      }
    }
  }
--]]
local backend = {}


function backend.get_player(p_name)
  return minetest.deserialize(storage:get_string(p_name)) or {completed = {}, active = {}}
end


function backend.set_player(p_name, data)
  storage:set_string(p_name, minetest.serialize(data))
end


function backend.get_player_completed_quests(p_name)
  return backend.get_player(p_name).completed or {}
end


function backend.get_player_active_quests(p_name)
  return backend.get_player(p_name).active or {}
end


function backend.start_quest(p_name, quest_id)
  local player = backend.get_player(p_name)
  local active = player.active
  if active[quest_id] then return false end

  active[quest_id] = {
    stage = 1
  }
  player.active = active
  backend.set_player(p_name, player)
  return true
end


function backend.set_quest_stage(p_name, quest_id, stage)
  local player = backend.get_player(p_name)
  local active = player.active
  if not active[quest_id] then return false end

  active[quest_id].stage = stage
  player.active = active
  backend.set_player(p_name, player)
  return true
end


--[[ Return values
-1: quest isn't active
0+: current stage
]]
function backend.get_quest_stage(p_name, quest_id)
  local player = backend.get_player(p_name)
  local active = player.active
  if not active[quest_id] then return -1 end

  return active[quest_id].stage
end


function backend.complete_quest(p_name, quest_id)
  local player = backend.get_player(p_name)
  local completed = player.completed
  if completed[quest_id] then return false end

  completed[quest_id] = true
  player.completed = completed
  player.active[quest_id] = nil
  backend.set_player(p_name, player)
  return true
end



return backend
