quests = {}
local modpath = minetest.get_modpath("quests")

quests.backend = dofile(modpath .. "/backend_storage.lua")
dofile(modpath .. "/api.lua")

quests.load_quests()
-- -- minetest.log(dump(quests.get_player_active_quests("SonoMichele")))
-- minetest.log("Prova next stage" .. quests.next_stage("SonoMichele", 2))
-- -- quests.complete_quest("SonoMichele", 1)
-- minetest.log("Completate " .. dump(quests.get_player_completed_quests("SonoMichele")))
-- -- local res, not_completed = quests.start_quest("SonoMichele", 2)
-- -- minetest.log("result: " .. res .. " " .. dump(not_completed))
-- minetest.log("Attive " .. dump(quests.get_player_active_quests("SonoMichele")))
